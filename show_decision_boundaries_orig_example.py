from mlxtend.plotting import plot_decision_regions
from mlxtend.preprocessing import shuffle_arrays_unison
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.svm import SVC

import numpy as np
import pandas as pd


# Loading some example data
iris = datasets.load_iris()
X, y = iris.data[:, [0, 2]], iris.target
X, y = shuffle_arrays_unison(arrays=[X, y], random_seed=3)

y = np.asarray(y).astype(np.int64)

X_train, y_train = X[:100], y[:100]
X_test, y_test = X[100:], y[100:]

# Training a classifier
svm = SVC(C=0.5, kernel='linear')
svm.fit(X_train, y_train)

# Plotting decision regions
plot_decision_regions(X, y.astype(np.int64), clf=svm, legend=2,
                      X_highlight=X_test)

# Adding axes annotations
plt.xlabel('sepal length [cm]')
plt.ylabel('petal length [cm]')
plt.title('SVM on Iris')
plt.show()
