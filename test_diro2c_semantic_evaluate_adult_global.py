import matplotlib.gridspec as gridspec
from mlxtend.plotting import plot_decision_regions
import matplotlib.pyplot as plt
from sklearn.datasets import make_classification
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import tree
from sklearn.model_selection import train_test_split
import _pickle as cPickle
import numpy as np
import pandas as pd
from sklearn.datasets import make_moons
from sklearn.metrics import f1_score
from sklearn.metrics import classification_report
from sklearn import svm
import copy
import seaborn as sn

import rule_extractor
import diro2c
from data import preprocess_data
from data_generation.neighborhood_generation import neighbor_generator
from data_generation.global_data_generation import global_data_generator
from data_generation.helper import *
import evaluation
from data.preprocess_data import _preprocess_adult_dataset
from data.preprocess_data import _preprocess_bank_marketing_dataset
from data.preprocess_data import _preprocess_credit_approval_dataset
from data import manipulate_data

from enums.dataset_type import dataset_type
from enums.diff_classifier_method_type import diff_classifier_method_type

from yellowbrick.model_selection import FeatureImportances


def main():

    dataset_name = 'adult.csv'
    path_data = './data/datasets/'
    dataset = _preprocess_adult_dataset(path_data, dataset_name)

    # dataset_name = 'bank-full.csv'
    # path_data = './data/datasets/'
    # dataset = _preprocess_bank_marketing_dataset(path_data, dataset_name)

    # dataset_name = 'credit_approval.csv'
    # path_data = './data/datasets/'
    # dataset = _preprocess_credit_approval_dataset(path_data, dataset_name)

    X1, y1 = dataset['X'], dataset['y']
    X1_train, X1_test, y1_train, y1_test = train_test_split(
        X1, y1, test_size=0.2, random_state=0)

    blackbox1 = DecisionTreeClassifier(random_state=0, max_depth=6)
    #blackbox1 = DecisionTreeClassifier(random_state=0)
    blackbox1.fit(X1_train, y1_train)

    print(classification_report(y1_test, blackbox1.predict(X1_test)))

    # print(dataset['columns_for_decision_rules'])
    fn = dataset['columns_for_decision_rules']
    cn = dataset['possible_outcomes']

    # fig1 = plt.figure(dpi=600)
    # tree.plot_tree(blackbox1,
    #                feature_names=fn,
    #                class_names=cn,
    #                filled=True)
    # fig1.savefig('images/semantic_evaluation/bb1_decision_tree.png')

    # viz = FeatureImportances(blackbox1)
    # viz.fit(X1_train, y1_train)
    # viz.show()

    # -----------------------------------------------
    # manipulate and train BB 2:

    #print(np.unique(X1[:, 11], return_counts=True))

    X2 = manipulate_data.manipulate_adult_hours_per_week(X1)
    y2 = copy.deepcopy(y1)

    X2_train, X2_test, y2_train, y2_test = train_test_split(
        X2, y2, test_size=0.2, random_state=0)

    blackbox2 = DecisionTreeClassifier(random_state=0, max_depth=6)
    #blackbox2 = DecisionTreeClassifier(random_state=0)
    blackbox2.fit(X2_train, y2_train)

    # fig2 = plt.figure(dpi=600)
    # tree.plot_tree(blackbox2,
    #                feature_names=fn,
    #                class_names=cn,
    #                filled=True)
    # fig2.savefig('images/semantic_evaluation/bb2_decision_tree.png')

    # covMatrix = np.corrcoef(dataset['df_encoded'].to_numpy(), bias=False)
    # sn.heatmap(np.around(covMatrix, 2), annot=True, fmt='g')
    # plt.show()

    idx = 34
    # print(X1_test[idx])

    # i = 0
    # for x in X1_test:
    #     if x[8] > 13000:
    #         print(i)
    #     i += 1

    X_to_recognize_diff = np.concatenate((X1_test, X2_test))

    diff_classifiers_info = diro2c.recognize_diff(idx, X_to_recognize_diff, dataset, blackbox1, blackbox2,
                                                  diff_classifier_method_type.multiclass_diff_classifier,
                                                  data_generation_function=global_data_generator.get_global_mod_genetic_neighborhood_dataset_experimental)

    # %matplotlib inline
    # gs = gridspec.GridSpec(3, 2)
    # fig = plt.figure(figsize=(14, 10))
    # labels = ['Logistic Regression', 'Decision Tree',
    #          'Random Forest', 'SVM', 'Naive Bayes', 'Neural Network']

    dc_info = diff_classifiers_info['multiclass_diff_classifer']
    dc_full = dc_info['dc_full']
    dc_test = dc_info['dc_test']
    evaluation_info = dc_info['evaluation_info']

    X_diff = evaluation_info['X']
    y_diff = evaluation_info['y']

    # diffs = 0
    # bb1_hit = 0
    # bb2_hit = 0
    # for xx, yy in zip(X_diff, y_diff):
    #     if yy == 1:
    #         if blackbox1.predict(np.array(xx).reshape(1, -1)) == 1:
    #             bb1_hit += 1
    #         diffs += 1

    # print('diffs: ', diffs)
    # print('bb1_hit: ', bb1_hit)
    # if diffs != 0:
    #     print('hit_ratio: ', bb1_hit / diffs)
    # else:
    #     print('hit_ratio: ', 0)

    # y_test_true = evaluation_info['y_test_true']
    # y_test_dc = evaluation_info['y_test_dc']

    # print(f1_score(y_test_true, y_test_dc))

    print(np.unique(y_diff, return_counts=True))

    #cn = ['no diff', 'diff']
    cn = ['00', '11', '10', '01']
    fig3 = plt.figure(dpi=600)
    tree.plot_tree(dc_full,
                   feature_names=fn,
                   class_names=cn,
                   filled=True)
    fig3.savefig('images/semantic_evaluation/dc_decision_tree.png')

    rule_extractor.print_rules_for_binary(
        dc_full, dataset['columns_for_decision_rules'], ['no diff', 'diff'], 'diff')


if __name__ == "__main__":
    main()
